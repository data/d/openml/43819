# OpenML dataset: Boston-Airbnb-Listings

https://www.openml.org/d/43819

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Since 2008, guests and hosts have used Airbnb to travel in a more unique, personalized way. As part of the Airbnb Inside initiative, this dataset describes the listing activity of homestays in Boston, MA. 
Content
This data file includes all needed information to about the listing details, the host, geographical availability, and necessary metrics to make predictions and draw conclusions. Basic data cleaning has been done, such as dropping redundant features (ex: city) and converting amenities into a dictionary. The data includes both numerical and categorical data, as well as natural language descriptions. 
Acknowledgements
This dataset is part of Airbnb Inside, and the original source can be found here.
Inspiration

Listing visualization
What features drive the price of a listing up?
What can we learn about different hosts and areas?
What can we learn from predictions? (ex: locations, prices, reviews, etc)
Which hosts are the busiest and why?
Is there any noticeable difference of traffic among different areas and what could be the reason for it?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43819) of an [OpenML dataset](https://www.openml.org/d/43819). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43819/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43819/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43819/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

